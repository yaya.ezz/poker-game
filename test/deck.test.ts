import request from 'supertest'
import {Express} from 'express-serve-static-core'
import {createServer} from './utils/server'
let server: Express

beforeAll(async () => {
    server = await createServer()
})




describe('Testing the Deck API', () => {
    const deckRequest = {
       type:'FULL',
        shuffled:true
    };
    let deckId;
    const count = 3
    it('Creat new Deck return deck obj with status 201', async () => {
        request(server).post(`api/deck/open/61f2c4ec8f1040576e3a51fd`)
            .set( 'content-type','application/x-www-form-urlencoded')
            .send(deckRequest)
            .expect(201)
            .end((err, res) => {

                expect(res.body.deckId).not.toBeNull();
                expect(res.body.type).not.toBeNull();
                expect(res.body.shuffled).not.toBeNull();
                expect(res.body.remaining).not.toBeNull();
                deckId = res.body.deckId
            })

    });

        it('Open Deck return deck with cards and status 200', async () => {
            request(server).get(`api/deck/open/${deckId}`)
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.deckId).not.toBeNull();
                    expect(res.body.type).not.toBeNull();
                    expect(res.body.shuffled).not.toBeNull();
                    expect(res.body.remaining).not.toBeNull();
                })
        })
    it('Draw card return all the drawn cards with status 200 ', async () => {
        request(server).get(`api/draw/card/${deckId}/${count}`)
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                expect(res.body).not.toHaveProperty('cards');
            })
    })

});