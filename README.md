# poker app

## Dependencies

- node 14
- npm
- docker

## Getting started
To install the dependencies 

```
npm install
```

To  make cope of .env file 

```
 cp .env.example .env
```



##run in container

Make sure the .env_docker file exists

To run the app on docker

```
docker-compose up
```

##run test

To run the test
```
npm run test
```
