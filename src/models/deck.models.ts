import { model, Schema, Model, Document } from "mongoose";

export interface IDeck extends Document {
  deckId: string;
  type: string;
  shuffled: boolean;
  remaining: number;
  cards: [{ value: string; suit: string; code: string }];
}

export const DeckSchema = new Schema({
  deckId: { type: String },
  type: { type: String },
  shuffled: { type: Schema.Types.Boolean },
  remaining: { type: Number },
  cards: [
    {
      value: { type: String },
      suit: { type: String },
      code: { type: String },
    },
  ],
});

export const Deck = model<IDeck>("Deck", DeckSchema);
