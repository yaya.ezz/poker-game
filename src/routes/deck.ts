
import express,{ Request, Response } from "express";
import controllers from '../controllers/deck.controller'
const router = express()
router.get('/', function (req, res) {
    res.send('hello')
})
router.post('/api/deck/create',controllers.createDeck )
router.get('/api/deck/open/:id',controllers.openDeck )
router.put('/api/draw/card/:id/:count',controllers.drawCard)

export default  router