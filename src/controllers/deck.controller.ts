import { Request, Response, NextFunction } from "express";
import getCards from "../lib/card";
import { Deck, IDeck } from "../models/deck.models";
import mongoose from "mongoose";

const createDeck = async (req: Request, res: Response, next: NextFunction) => {
  let deckId = new mongoose.Types.ObjectId(),
    type: string = req.body.type,
    shuffled: boolean = req.body.shuffled;

  if (!type || !shuffled) {
    return res.status(400).json({
      message: "some data are missing",
    });
  }
  let cards = getCards(type, shuffled);
  try {
    let data: IDeck = await Deck.create({
      deckId,
      type,
      shuffled,
      remaining: cards.length,
      cards,
    });

    return res.status(201).json({

        deckId: data.deckId,
        type: data.type,
        shuffled: data.shuffled,
        remaining: data.remaining,

    });
  } catch (error) {
    console.log(error);
  }
};

const openDeck = async (req: Request, res: Response, next: NextFunction) => {
  let deckId: string = req.params.id;
  if (!deckId) {
    return res.status(400).json({
      message: "some data are missing",
    });
  }
  
  let data: IDeck = await Deck.findOne({
    deckId,
  });

  res.status(200).json({

      deckId: data.deckId,
      type: data.type,
      shuffled: data.shuffled,
      remaining: data.remaining,
      cards: data.cards,

  });
};
const drawCard = async (req: Request, res: Response, next: NextFunction) => {
  let deckId = req.params.id,
    count = parseInt(req.params.count);
  let data: IDeck = await Deck.findOne({
    deckId,
  });
  let removedCards = [];
  for (let i: number = 0; i < count; i++) {
    removedCards.push(data.cards[0]);
    data.cards.shift();
  }
 await Deck.findOneAndUpdate(
    { deckId },
    { cards: data.cards, remaining: data.cards.length },
    {
      new: true,
    }
  );

  res.status(200).json({
     cards:removedCards
  });
};

export default { createDeck, openDeck, drawCard };
