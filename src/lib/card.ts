export default function getCards(type: string, shuffled: boolean) {
  const suits: string[] = ["SPADES", "CLUNS", "DIAMONDS", "HEARTS"];
  const values: string[] = [
    "ACE",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "JACK",
    "QUEEN",
    "KING",
  ];
  let deck = [];

  // create a deck of cards
  for (let i = 0; i < suits.length; i++) {
    for (let x = 0; x < values.length; x++) {
      let card = {
        value: values[x],
        suit: suits[i],
        code: `${values[x].charAt(0)}${suits[i].charAt(0)}`,
      };
      if (type === "SHORT") {
        if (deck.length >= 36) {
          continue;
        }
      }
      deck.push(card);
    }
  }
  // shuffle the cards
  if (shuffled === true) {
    console.log(shuffled);
    for (let i = deck.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * i);
      let temp: any = deck[i];
      deck[i] = deck[j];
      deck[j] = temp;
    }
  }

  return deck;
}
