import http from "http";
import express, { Express } from "express";
import routes from "./routes/deck";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import dotenv from "dotenv";
dotenv.config();
const app: Express = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/", routes);
app.use((req, res, next) => {
  const error = new Error("not found");
  return res.status(404).json({
    message: error.message,
  });
});
const url = process.env.DB_URL || 'mongodb://localhost:27017/poker'
mongoose.connect(process.env.DB_URL);
const connection = mongoose.connection;

connection.once("open", function () {
  console.log("MongoDB database connection established successfully");
});
const httpServer = http.createServer(app);
const PORT: any = process.env.PORT|| 3000;
const hostname: string = process.env.HOST_NAME|| '0.0.0.0';

httpServer.listen(PORT, hostname, () =>
  console.log(`The server is running on: http://${hostname}:${PORT}`)
);
